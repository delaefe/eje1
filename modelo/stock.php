<?php

require_once 'bd.php';

Class Stock{

    //este objeto debe tener los campos de la tabla persona como variables
    private $idregistrostock;
    private $idproducto;
    private $sucursal;
    private $cantidad;

    private $pdo;

    public function __CONSTRUCT(){
        $this->pdo = BD::Conectar();
    } 

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -/
    * F U N C I O N E S    C R U D  - - - - - - - - - - - - - - - - - - - - - - - -/
    *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /**
     */
    public function Insertar(Stock $stk){

        try{ //todo: cambiar parametros con objeto actual
            //los dos ?? serán reemplazados por dos datos (pasados como array)
            //en la tabla gestionstock.stock la variable idregistrostock es autoincrementable
            $consulta = "insert into stock(idproducto, sucursal, cantidad) values (?, ?, ?)";
            //prepara la consulta, reemplazando ? por las variables
            $this->pdo->prepare($consulta)->execute( array($stk->getIdproducto(), $stk->getSucursal(), $stk->getCantidad() ) ) ;
        } catch (Exception $e) {
            die("error : ".$e->getMessage() );
        }
    }

    public function Listar(){
        try{
            //esta tabla es una vista (descrita más abajo)
            $consulta = $this->pdo->prepare("SELECT * FROM stockCompleto");
            $consulta->execute();
            return $consulta->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die("error : ".$e->getMessage());
        }
    }

    //GETTERS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    /**
     * Get the value of idregistrostock  */
    public function getIdregistrostock() :?string{  return $this->idregistrostock; }
    
    /**
     * Get the value of idproducto  */
    public function getIdproducto() :?string{       return $this->idproducto; }

    /**
     * Get the value of sucursal  */
    public function getSucursal() :?string{         return $this->sucursal; }

    /**
     * Get the value of cantidad  */
    public function getCantidad() :?string{         return $this->cantidad; }

    //SETTERS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    /**
     * Set the value of idregistrostock */
    public function setIdregistrostock($idregistrostock): self{
        $this->idregistrostock = $idregistrostock;
        return $this;
    }

    /**
     * Set the value of idproducto  */
    public function setIdproducto($idproducto): self{
        $this->idproducto = $idproducto;
        return $this;
    }

    /**
     * Set the value of sucursal    */
    public function setSucursal($sucursal): self{
        $this->sucursal = $sucursal;
        return $this;
    }

    /**
     * Set the value of cantidad    */
    public function setCantidad($cantidad): self{
        $this->cantidad = $cantidad;
        return $this;
    }
}

/*
CREATE VIEW stockCompleto AS
SELECT productos.nombre AS Producto, 
productos.precio AS Precio_unidad, 
categorias.nombre AS Categoría, 
stock.cantidad, 
sucursales.nombre AS Sucursal 
FROM  (productos 
    INNER JOIN stock ON stock.idproducto = productos.idproducto 
    INNER JOIN sucursales ON stock.sucursal=sucursales.idSucursal 
    INNER JOIN categorias ON categorias.idcategoria=productos.categoria);
*/