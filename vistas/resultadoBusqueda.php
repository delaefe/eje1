<html>

<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossorigin="anonymous">
        <link rel="stylesheet" href="styles/estilos.css">
</head>

<body>

    <!--TABLA PARA MOSTRAR EL RESULTADO DE LA BÚSQUEDA
    obenemos los datos desde la variable $producto (tipo PRODUCTO)
    venimos de controlador/producto.controlador.php -> buscar()
    -->
    <table class="table">
        <thead>
            <tr>
                <th scope="col">idproducto</th>
                <th scope="col">Nombre</th>
                <th scope="col">Categoría</th>
                <th scope="col">Descripción</th>
                <th colspan="2">Opciones...</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td> <?= $producto->getIdproducto(); ?>  </td>
                <td> <?= $producto->getNombre(); ?>      </td>
                <td> <?= $producto->getCategoria(); ?>   </td>
                <td> <?= $producto->getDescripcion(); ?> </td>
                <td> <a class="btn btn-secondary" href="?path=producto&metodo=FormEditarProducto&id=<?=$producto->getIdproducto(); ?>">Editar producto</button> </td>
                <td> <a class="btn btn-danger" href="?path=producto&metodo=FormBorrarProducto&id=<?=$producto->getIdproducto(); ?>">Borrar producto</button> </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
