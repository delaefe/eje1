<?php

require_once "modelo/stock.php";
require_once 'console_log.php';

class AdministrarstockControlador{

    private $modelo;
    public function __CONSTRUCT(){
        echo "Controlador consultar producto";
        $this->modelo = new Stock();
    }

    public function inicio(){
        //llama a la vista de inicio 
        require_once "vistas/cabecera.php";
        require_once "vistas/administrarStock.php";
    }

    public function ingresar(){

        print " ingresando nuevo stock ";

        //creamos nuevo obj stock y asignamos valores.
        $stock = new Stock();
        $stock->setIdproducto($_POST["producto"]);
        $stock->setSucursal($_POST["sucursal"]);
        $stock->setCantidad($_POST["cantidad"]);

        //lo ingresamos a la bd
        $this->modelo->Insertar($stock);

        //redirigimos a la vista de la lista
        $this->Listar();
    }

    public function Listar(){
        require_once "vistas/cabecera.php";
        require_once "vistas/listadoStock.php";
    }


}
