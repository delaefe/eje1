<?php
    require_once 'console_log.php';

    //si variable controler viene vacía, redirige a inicio
    if(!isset($_GET['controller'])){

        console_log ("\nINDEX.PHP: sin params controller");

        //llama al controlador inicial
        require_once "controlador/inicio.controlador.php";
        $controlador = new InicioControlador();

        //ejecuta la funcion inicio del objeto controlador
        call_user_func(array($controlador,"inicio"));
    }else{

        //el usuario indica una opción 
        $controlador = $_GET["controller"];
        console_log ("\nINDEX.PHP: ruta a : ".$controlador);

        require_once "controlador/$controlador.controlador.php";

        console_log("\ncontrolador: ".$controlador);

        $controlador = $controlador."Controlador";
        $controlador = new $controlador;
//        console_log("\ncontrolador: ".$controlador);

        //
        if(isset($_GET["metodo"])){
            $metodo = $_GET["metodo"];
        }else{
            $metodo = "inicio";
        }
        call_user_func(array($controlador,$metodo));

    }
