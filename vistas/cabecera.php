<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="js/validarDatos.js" ></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossorigin="anonymous">
        <link rel="stylesheet" href="styles/estilos.css">
    <title>Administrador de Stock</title>
</head>

</body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class=" navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="?controller=inicio">Inicio</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="?controller=inicio&metodo=registrarProducto">Registrar nuevo producto </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?controller=inicio&metodo=consultarProducto">Consultar datos de productos</a>
                </li>      
                <li class="nav-item ">
                    <a class="nav-link" href="?controller=inicio&metodo=consultarStock">Administrar Stock en sucursales</a>
                </li>
          
            </ul>
        </div>
    </nav>

 
</body>

</html>
