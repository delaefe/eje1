<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossorigin="anonymous">
    <title>Administrador de Stock</title>
        <link rel="stylesheet" href="../styles/estilos.css">
</head>

</body>
<div id="page-container">

    <?php
        //query para obtener todas las categorías desde la base de datos
        include "modelo/Conexion.php";
        $conexion = new Conexion();
        $con = $conexion->conecta();
        $querySucursales = "select * from sucursales;";
        $listaSucursales = mysqli_query($con, $querySucursales);
    ?>

    <div class="col-md-8 mx-auto">

        <h1 class="titulos">Consultar un producto</h1>

        <div class="cajaCentrada">

        <br>
        <h2 class="titulos">Indique parámetros de búsqueda </h2>
        <br>
        <!--action="php/ingresaDatos.php" method="POST"-->
        <form action="?controller=producto&metodo=buscar" method="POST" onsubmit="return validarDatosConsulta()"> 

            <div class="form-group">
                <label for="nombreProducto">Nombre del producto</label>
                <input type="text" class="form-control" id="nombreProducto" name="nombreProducto" ></input>        
            </div>
            <br>

            <div class="form-group">
                <label for="codigoUnico">Código único</label>
                <input type="text" class="form-control" id="idproducto" name="idproducto"></input>        
            </div>
            <br>

            <div class="form-group">
                <label for="selSucursal">Sucursal</label>
                <select name="sucursal" class="form-control" id="selSucursal">
                    <option value="">seleccione sucursal</option>
                    <?php 
                        //generamos todas las opciones de las sucursales
                        foreach( $listaSucursales as $suc){
                            print  "<option value=".$suc["idSucursal"].">".$suc["nombre"]."</option>";
                        }
                    ?>
                </select>
            </div>
            <br>
            
            <hr>
                <div class="info">
                <small>Formulario action="controller=producto&metodo=buscar"</small><br></div>
            <input type="submit" class="btn btn-primary btnSubmit" value="Consultar"></input> 
                <div class="info">
                <small> Los datos del formulario serán recibidos en controlador/producto.controlador.php ->buscar() </small> </div>
        </form>
        </div>
    </div>

    <div class="col-md-8 mx-auto">
        <div class="cajaCentrada">
            <h1>Listar todos los productos </h1>    

            <form action="?controller=producto&metodo=listar" method="POST" > 
                <br>
                <div class="info"><small>Formulario action="?controller=producto&metodo=listar"</small></div>
                <input type="submit" class="btn btn-primary btnSubmit" value="Consultar"></input> 
                <div class="info"><small> Los datos del formulario serán recibidos en controlador/producto.controlador.php ->listar() </small> </div>
            </form>
        </div> 
    </div>    

    <footer class="text-center">
        <small>Desarrollado por  Diego de la Fuente Curaqueo</small>
    </footer>
</div>
</body>

</html>
