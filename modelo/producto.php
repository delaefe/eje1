<?php

require_once 'bd.php';
require_once 'console_log.php';

Class Producto{

    //este objeto debe tener los campos de la tabla persona como variables
    private $nombre;
    private $precio;
    private $descripcion;
    private $categoria;
    private $idproducto;

    private $pdo;

    public function __CONSTRUCT(){
        console_log("\nCREANDO OBJETO PRODUCTO");
        $this->pdo = BD::Conectar();
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -/
    * F U N C I O N E S    C R U D  - - - - - - - - - - - - - - - - - - - - - - - -/
    *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    /**
     */
    public function Insertar(Producto $producto){
        try{ //todo: cambiar parametros con objeto actual
            //los dos ?? serán reemplazados por dos datos (pasados como array)
            //en la tabla gestionstock.productos la variable idproducto es autoincrementable
            $consulta = "insert into productos(nombre, precio, descripcion, categoria) values (?, ?, ?, ?)";
            //prepara la consulta, reemplazando ? por las variables
            $this->pdo->prepare($consulta)->execute( array($producto->getNombre(), $producto->getPrecio(), $producto->getDescripcion(), $producto->getCategoria() ) ) ;

            //header("location:index.php");
        } catch (Exception $e) {
            die("error : ".$e->getMessage() );
        }
    }

    public function BuscarPorNombre($nombre){
        try{
            $consulta = $this->pdo->prepare("SELECT * FROM productos where nombre=?;");
            $consulta->execute(array($nombre));
            return $consulta->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die("error : ".$e->getMessage());
        }
    }

    public function BuscarPorSucursal($nombre){
        try{
            $query = "select productos.nombre, productos.precio, productos.descripcion, productos.categoria, productos.idproducto from productos join stock on stock.idproducto = productos.idproducto join sucursales on sucursales.idsucursal=stock.sucursal where sucursales.idSucursal=?;";
            $consulta = $this->pdo->prepare($query);
            $consulta->execute(array($nombre));
            return $consulta->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die("error : ".$e->getMessage());
        }
    }

    /** 
     */
    public function Listar(){
        try{
            $consulta = $this->pdo->prepare("SELECT * FROM productos");
            $consulta->execute();
            return $consulta->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die("error : ".$e->getMessage());
        }
    }

    /**
     */
    public function BuscarProducto($idproducto){
        //esto lo vimos en clases;
        console_log("\nBuscando producto ".$idproducto);
        try{
            $consulta = $this->pdo->prepare("select * from productos where idProducto = ?;");
            $consulta->execute(array($idproducto));
            $resultado = $consulta->fetch(PDO::FETCH_OBJ); //parece que está pasando un obj plano (s/metodos)

            $producto = new Producto();
            $producto->setNombre($resultado->nombre); //como propiedad sí funciona
            $producto->setIdproducto($idproducto);
            $producto->setDescripcion($resultado->descripcion);// como getter no funciona
            $producto->setCategoria($resultado->categoria);
            $producto->setPrecio($resultado->precio);

            return $producto;

        }catch(Exception $e){
            die($e->getMessage());
        }
    }

    /**
     */
    public function Borrar($idproducto){
        try{
            console_log("\nIntentando borrar producto con id ".$idproducto);
            $consulta = $this->pdo->prepare("delete from productos where idproducto=?;");
            $consulta->execute(array($idproducto));
            //$resultado = $consulta->fetch(PDO::FETCH_OBJ); //parece que está pasando un obj plano (s/metodos)
        }catch(Exception $e){
            die($e->getMessage());
        }
    }

    /**
     */
    public function Actualizar(Producto $producto){
        try{
            $consulta = "UPDATE productos SET idproducto=?, nombre=?, precio=?, descripcion=? WHERE idproducto=?";
            console_log("\n Intentanod realizar contula actualizar: ".$consulta);
            $this->pdo->prepare($consulta)->execute(array($producto->getIdproducto(), $producto->getNombre(), $producto->getPrecio(), $producto->getDescripcion(), $producto->getIdproducto()));
        }catch(Exception $e){
            die($e->getMessage());
        }    
    }
    
    //GETTERS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    /**
     */
    public function getRut() :?string {     return $this->rut;}

    /**
     */
    public function getNombre() :?string {  return $this->nombre;}

    /**Get the value of categoria
     */
    public function getCategoria(){         return $this->categoria;}

     /**Get the value of precio
     */
    public function getPrecio() :?int{            
        return intval($this->precio);}

     /**Get the value of idproducto
     */
    public function getIdproducto(){        return $this->idproducto;}

    /**Get the value of descripcion
     */
    public function getDescripcion(){       return $this->descripcion;}


    //SETTERS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    /**  
     */    
    public function setRut(string $rut) {       $this->rut = $rut;      }

    /** 
     */
    public function setNombre(string $nombre) { $this->nombre = $nombre; }

    /** Set the value of categoria
     */
    public function setCategoria($categoria): self{
        $this->categoria = $categoria;
        return $this;
    }

    /** Set the value of descripcion
     */
    public function setDescripcion($descripcion): self{
        $this->descripcion = $descripcion;
        return $this;
    }

    /** Set the value of precio
     */
    public function setPrecio($precio): self{
        $this->precio = $precio;
        return $this;
    }

    /** Set the value of idproducto
     */
    public function setIdproducto($idproducto): self{
        $this->idproducto = $idproducto;
        return $this;
    }
}
