<?php
require_once "modelo/producto.php";
require_once 'console_log.php';

class ProductoControlador{

    private $modelo;
    public function __CONSTRUCT(){
        console_log (" producto.controlador: Controlador producto");
        $this->modelo = new Producto();
    }

    public function inicio(){
        //llama a la vista de inicio 
        require_once "vistas/cabecera.php";
        require_once "vistas/registrarProducto.php";
    }

    /** Ingresa un producto a la base de datos
     */
    public function ingresar(){
        
        console_log( " ingresando nueva producto " );
        $producto = new Producto();
        $producto->setNombre( $_POST["nombreProducto"] );
        $producto->setPrecio( $_POST["precio"] );
        $producto->setCategoria( $_POST["categoria"] );
        $producto->setDescripcion( $_POST["descripcion"] );

        //ingresamos el nuevo registro utilizando modelo/producto.php
        $this->modelo->Insertar( $producto );
        
        //luego listamos
        $this->listar();
    }

    public function FormEditarProducto(){
        //console_log( isset($_GET) );
        $producto = new Producto();
        $producto = $this->modelo->BuscarProducto( intval($_GET["id"]) );
        require_once "vistas/cabecera.php";
        require_once "vistas/FormEditarProducto.php";
    }

    public function FormBorrarProducto(){
        console_log( "\nFormBorrarProducto ".$_GET["id"]);

        //borra el producto con la id indicada
        $this->modelo->Borrar( $_GET["id"] );

        //vuelve a la vista principal con la lista de productos
        $this->listar();
    }

    public function buscar(){
        console_log("PRODUCTO.CONTROLADOR: BUSCAR()");

        $indicaId       = !empty( $_POST["idproducto"] ); // "" == trim( $_POST["idproducto"] )  ;
        $indicaNombre   = !empty( $_POST["nombreProducto"] ); // "" == trim($_POST["nombreProducto"] ) ;
        $indicaSucursal = !empty( $_POST["sucursal"] ) ;

        console_log( $indicaNombre );
        console_log( $indicaId ); 
        console_log( $indicaSucursal );

        $producto = new Producto();

        /*3 tipos de búsqueda
        * A) SOLO INDICA ID DE BÚSQUEDA                 */ 
        if($indicaId){
            console_log("id");
            //creamos un producto nuevo y le asignamos el producto buscado
            $producto = $this->modelo->BuscarProducto($_POST["idproducto"]);
            //redirige a la página de resutlado, donde se muestran los datos de la variable $proucto
            require_once "vistas/cabecera.php";
            require_once "vistas/resultadoBusqueda.php";

        //B) INDICA NOMBRE
        }else if($indicaNombre){
            console_log("nombre");
            $productos = new Producto();
            $productos = $this->modelo->BuscarPorNombre($_POST["nombreProducto"]);
            //redirige a la página de resutlado, donde se muestran los datos de la variable $proucto
            require_once "vistas/cabecera.php";
            require_once "vistas/listadoBusqueda.php";
        }else if($_indicaSucursal){
            console_log("sucursal");
            $productos = new Producto();
            $productos = $this->modelo->BuscarPorSucursal($_POST["sucursal"]);
            //redirige a la página de resutlado, donde se muestran los datos de la variable $proucto
            require_once "vistas/cabecera.php";
            require_once "vistas/listadoBusqueda.php"; 
        }
    }

    public function listar(){
        console_log( isset($_GET) );
        console_log( isset($_POST) );

        $productos = $this->modelo->Listar();
        console_log($productos);

        require_once "vistas/cabecera.php";
        require_once "vistas/listadoBusqueda.php";
    }


    public function modificar(){

        $producto = new Producto();
        $producto->setNombre( $_POST["nombre"]);
        $producto->setPrecio( $_POST["precio"]);
        $producto->setDescripcion( $_POST["descripcion"]);
        $producto->setCategoria( $_POST["categoria"]);
        $producto->setIdproducto( $_POST["idproducto"]);

        $this->modelo->Actualizar($producto);
            
        require_once "vistas/cabecera.php";
        require_once "vistas/listadoBusqueda.php";
    }

}