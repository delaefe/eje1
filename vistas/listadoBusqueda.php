<html>

    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                crossorigin="anonymous">
        <link rel="stylesheet" href="styles/estilos.css">
    </head>

    <body>

        <!--TABLA PARA MOSTRAR LA LISTA DE PRODUCTOS-->

        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">idproducto</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Categoría</th>
                    <th scope="col">Descripción</th>
                    <th colspan="2">Opciones...</th>

                </tr> 
            </thead>
            <tbody>
                    <?php
                        //obtenemos la lista de registros desde el modelo/producto.php -> Listar() 
                        //luego iteramos para asignar los valores a cada celda de la tabla.
                        //(venimos desde consulta.controlador.php -> listar())
                        $registros = $this->modelo->Listar();
                        foreach($registros as $row){
                    ?>
                        <tr>
                            <td id=<?= "td_idprod_id".$row->idProducto; ?> > <?= $row->idProducto; ?>  </td>
                            <td id=<?= "td_nombre_id".$row->idProducto; ?>> <?= $row->nombre; ?>      </td>
                            <td id=<?= "td_precio_id".$row->idProducto; ?>> <?= $row->precio; ?>      </td>
                            <td id=<?= "td_categoria_id".$row->idProducto; ?>> <?= $row->categoria; ?>   </td>
                            <td id=<?= "td_descripcion_id".$row->idProducto; ?>> <?= $row->descripcion; ?> </td>
                            <td id=<?= "td_editar_id".$row->idProducto; ?>> <a class="btn btn-secondary" href="?controller=producto&metodo=FormEditarProducto&id=<?=$row->idProducto;?>">Editar producto</button> </td>
                            <td id=<?= "td_borrar_id".$row->idProducto; ?> onclick="return validarBorrar(this.id);"> <a class="btn btn-danger"    href="?controller=producto&metodo=FormBorrarProducto&id=<?=$row->idProducto;?>">Borrar producto</button> </td>
                        </tr>
                    <?php
                        }
                    ?>
            </tbody>
        </table>

    </body>

</html>

