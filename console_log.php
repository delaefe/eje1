<?php

//esta función simula un console.log de js. Muy util pa debugear... eso...
function console_log($output, $incluirTags = true) {
    $codigoJS = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
    if ($incluirTags) {
        $codigoJS = '<script>' . $codigoJS . '</script>';
    }
    echo $codigoJS;
}