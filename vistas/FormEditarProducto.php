<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossorigin="anonymous">
    <link rel="stylesheet" href="../styles/estilos.css">

    <title>Administrador de Stock</title>

</head>

</body>
<div id="page-container">

    <?php
        //query para obtener todas las categorías desde la base de datos
        include "modelo/Conexion.php";
        $conexion = new Conexion();
        $con = $conexion->conecta();
        $queryCategoria = "select * from categorias;";
        $listaCategoria = mysqli_query($con, $queryCategoria);
    ?>

    <h1 class="titulos"> Actualizar datos de producto</h1>
        <div class="col-md-8 mx-auto">
            <div class="cajaCentrada">
                <form action="?controller=producto&metodo=modificar" method="POST" onsubmit="return validarDatosRegistro()"> 
                    <div class="form-group">
                        <label for="nombreProducto">Nombre del producto</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" value="<?= $producto->getNombre(); ?>" required></input>        
                        <br>
                    </div>

                    <div class="form-group">
                        <label for="idproducto">idProducto</label>
                        <input type="text" class="form-control" id="idproducto" name="idproducto" value="<?= $producto->getIdproducto(); ?>" required></input>        
                        <br>
                    </div> 

                    <div class="form-group">
                        <label for="categoria">Categoria</label>
                        <select name="categoria" class="form-control" id="selCategoria" required>
                            <option value="">seleccione categoria</option>
                            <?php 
                                //creamos un option por cada categoría obtenida desde la base de datos (arriba)
                                foreach( $listaCategoria as $prod){
                                    //ingresa categoría por categoría. Dejando la seleccionada según query
                                    if( $producto->getCategoria() == $prod["idCategoria"])
                                        print  "<option value=".$prod["idCategoria"]." selected >".$prod["nombre"]."</option>";
                                    else{
                                        print "<option value=".$prod["idCategoria"].">".$prod["nombre"]."</option>";
                                    }
                                }
                            ?>
                        </select>
                        <br>
                    </div> 

                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" value="<?= $producto->getDescripcion(); ?>"  required></input>        
                        <br>
                    </div>

                    <div class="form-group">
                        <label for="precio">Precio</label>
                        <input type="number" class="form-control" id="precio" name="precio" min="0" value="<?=intval($producto->getPrecio());?>" required></input>        
                        <br><hr>
                     </div>
                     <div class="info"><small>Formulario action="?controller=producto&metodo=modificar"</small></div>
                    <input type="submit" class="btn btn-primary btnSubmit" value="Actualizar"></input> 
                    <div class="info"><small> Los datos del formulario serán recibidos en controlador/producto.controlador.php -> modificar() </small> </div>
                </form>
            </div>
        </div>

    <footer class="text-center">
        <small>Desarrollado por Diego de la Fuente Curaqueo</small>
    </footer>
</div>
</body>

</html>
