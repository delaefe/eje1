/**
 * @param {string} str String a verificar */
function validarString(str){
    if(str==undefined || str==null || str.length == 0 || /\s/.test(str) || str == "" ){
        return false;
    }else{
        return true;
    }
}

/**
 * @param {number} n String a convertir a número y verificar */
function validarNumero(n){
    numero = parseInt(n);
    if( isNaN(numero) || numero < 0){
        return false;
    }else{
        return true;
    }
}

function validarCamposLogIn(){

    let log = {
        user: document.getElementById("user").value,
        pass: document.getElementById("pass").value
    }

    for(let campo of Object.keys(log)){
        if(!validarString(log[campo])){
            alert("Debe indicar dato correcto "+campo);
            document.getElementById(campo).focus();
            return false;
        }
    }
}

/**
 * Valida datos para registar un nuevo producto   */
function validarDatosRegistro(){

    //creamos un objeto con todo el formulario
    let formulario = {  
        codigoUnico:      document.getElementById("codigoUnico").value,
        nombreProducto :  document.getElementById("nombreProducto").value,
        categoria:        document.getElementById("selCategoria").value,
        precio:           document.getElementById("precio").value
    }

    //verificamos cada key del objeto, distinguiendo entre números y strings
    for(let campo of Object.keys(formulario)){

        //valida numeros
        if(campo == "cantidad" || campo =="precio"){ 
            if(!validarNumero(formulario[campo])){
                alert("Ingrese un número válido en "+campo)
                document.getElementById(campo).focus();
                return false; 
            }
        //valida Strings
        }else if(!validarString(formulario[campo])){ 
            alert("ingrese dato valido en "+campo)
            document.getElementById(campo).focus();
            return false;        }
    }
}

/**
 * Validar datos de consulta de producto */
function validarDatosConsulta(){
//creamos un objeto con todo el formulario
    let formulario = {  
        codigoUnico:      document.getElementById("idproducto").value,
        nombreProducto :  document.getElementById("nombreProducto").value,
        sucursal:         document.getElementById("selSucursal").value
    }

    console.log("fomr: "+formulario);

    let cont = 0;
    for(let campo of Object.keys(formulario)){
        if(validarString(formulario[campo])){
            console.log(campo);
            cont++;
        }
    }
    console.log(cont);
    if(cont == 0){
        alert ("Debe indicar al menos un dato para realizar la consulta");
        return false
    }
}

const validarDatosStock=()=>{
   //creamos un objeto con todo el formulario
    let formulario = {  
        selProducto:    document.getElementById("selProducto").value,
        selSucursal :   document.getElementById("selSucursal").value,
        cantidad:       document.getElementById("cantidad").value
    } 

}

const validarBorrar = (idprod) =>{

    let nombre      =  document.getElementById("td_nombre_id" + idprod.split("id")[1]).innerText;
    let idproducto  =  document.getElementById("td_idprod_id" + idprod.split("id")[1]).innerText;
    let categoria   =  document.getElementById("td_categoria_id" + idprod.split("id")[1]).innerText;
    let precio      =  document.getElementById("td_precio_id" + idprod.split("id")[1]).innerText;

    let confirmar = confirm(`¿Está seguro de que quiere borrar el registro id.${idproducto}? \n(Esta opción no se puede deshacer)\n\n Nombre: ${nombre} \n Categoría: ${categoria} \n Precio: ${precio}`);

    return confirmar;
 
}

/**
 * Validar datos de búsqueda de producto para actualizar sus datos (backend) */
function validarCamposBuscar(){
    let productoBuscado = document.getElementById("codigoUnicoBuscar").value;

    if(validarString(productoBuscado)){

    }else{
        alert("debe indicar un código único");
        document.getElementById("codigoUnicoBuscar").focus();
        return false;
    }

}

/**
 * Validar datos para actualizar producto buscado  */
function validarCamposActualizar(){

    let codigoUnicoBuscar = document.getElementById("codigoUnicoBuscar").value;

    if(validarString(codigoUnicoBuscar)){

    }else{
        alert("debe indicar un código único");
        document.getElementById("codigoUnicoBuscar").focus();
        return false;
    }
    // ingreso de datos....

}