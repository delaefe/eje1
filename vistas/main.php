<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossorigin="anonymous">
        <link rel="stylesheet" href="styles/estilos.css">
    <title>Administrador de Stock</title>
</head>

</body>
<div id="page-container">

    <h1 class="titulos">Iniciar sesión</h1><small>*(pendiente)</small>
    <br><br><hr>

    <div class="container">
        <h2>Modelo BD</h2>
        <code class="codigo">
        mysql> desc productos;   
        +-------------+-------------+------+-----+---------+----------------+
        | Field       | Type        | Null | Key | Default | Extra          |
        +-------------+-------------+------+-----+---------+----------------+
:······>| idProducto  | int         | NO   | PRI | NULL    | auto_increment |
:       | nombre      | varchar(12) | YES  |     | NULL    |                |
:       | precio      | int         | YES  |     | NULL    |                |
:       | descripcion | varchar(50) | YES  |     | NULL    |                |
:   :···| categoria   | int         | YES  | MUL | NULL    |                |
:   :   +-------------+-------------+------+-----+---------+----------------+
:   :
:   :   mysql> desc categorias;
:   :   +-------------+-------------+------+-----+---------+----------------+
:   :   | Field       | Type        | Null | Key | Default | Extra          |
:   :   +-------------+-------------+------+-----+---------+----------------+
:   :··>| idCategoria | int         | NO   | PRI | NULL    | auto_increment |
:       | nombre      | varchar(12) | YES  |     | NULL    |                |
:       +-------------+-------------+------+-----+---------+----------------+
:    
:       mysql> desc stock;
:       +-----------------+------+------+-----+---------+----------------+
:       | Field           | Type | Null | Key | Default | Extra          |
:       +-----------------+------+------+-----+---------+----------------+
:       | idRegistroStock | int  | NO   | PRI | NULL    | auto_increment |
:·······| idProducto      | int  | YES  | MUL | NULL    |                |
    :···| sucursal        | int  | YES  | MUL | NULL    |                |      
    :   | cantidad        | int  | YES  |     | NULL    |                |
    :   +-----------------+------+------+-----+---------+----------------+
    :
    :   mysql> desc sucursales;
    :   +------------+-------------+------+-----+---------+----------------+
    :   | Field      | Type        | Null | Key | Default | Extra          |
    :   +------------+-------------+------+-----+---------+----------------+
    :··>| idSucursal | int         | NO   | PRI | NULL    | auto_increment |
        | nombre     | varchar(12) | YES  |     | NULL    |                |
        +------------+-------------+------+-----+---------+----------------+
        </code>

    </div>

    <div class="container">
        <p>*Este trabajo incluye los items de las actividades 1, 2 y 3. Así como también las Evaluaciones 1, 2, 3 y final.</p>
        <h3>​Diseñar un software gestión de stock: <br><small>(Requerimientos generales)</small> </h3>
        <ul>
            <li>Registrar un producto con los siguientes datos, id auto incrementable, código único del producto, nombre del producto, categoría, sucursal en la que se encuentra, y descripción, cantidad, y precio venta. </li>
            <li>Asignar productos a sucursal. (Defina 3 sucursales.) </li>
            <li>Consultar productos por código, nombre, y opcionalmente la sucursal. </li>
            <li>Dar de baja un producto. (Con la opción de eliminar el producto). </li>
            <li>Actualizar nombre, precio y descripción del producto. </li>
            <li>Crear un login para administrador del sistema. </li>
        </ul>

        <h4> <strong>ACTIVIDAD 1 </strong> - Se solicita: </h4>
        <ul>
            <li>Cree las vistas que satisfagan los requerimientos (View) un entorno de desarrollo a selección (Netbeans o VS Code o similar), para ello cree un proyecto inicial, con una página de inicio y un dashboard para las opciones. </li>
        </ul>

        <h4><strong>EVALUACIÓN 1 </strong>- Se solicita:</h4>
        <ol>
            <li> Diseñe un estilo visual o utilice una plantilla Bootstrap que se ajuste a los requerimientos.</li> 
            <li> Valide los campos obligatorios de las vistas y envíelos a una clase PHP la cual mostrará los datos por pantalla. </li>
        </ol>


        <h4> <strong>EVALUACIÓN 2  </strong> - Se solicita: </h4>
        <ol>
            <li> Insertar el registro de un producto nuevo en la base de datos.  </li> 
            <li> Registrar un producto asociado a una sucursal.  </li> 
        </ol>


        <h4> <strong>ACTIVIDAD 3</strong> - Se solicita: </h4>
        <ol>
            <li> Realizar la búsqueda de producto por código o nombre y opcionalmente la sucursal y mostrar los resultados.  </li> 
        </ol>
        
        <h4><strong>EVALUACIÓN 3  (actual y final)</strong> - Se solicita: </h4>
        <ol>
            <li> actualizar y borrar </li> 
        </ol>
    </div>




    <footer class="text-center">
        <small>Desarrollado por  Diego de la Fuente Curaqueo</small>
    </footer>
</div>
</body>

</html>
