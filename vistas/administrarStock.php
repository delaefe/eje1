<html>
<?require_once 'console_log.php';?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossorigin="anonymous">
    <title>Administrador de Stock</title>
        <link rel="stylesheet" href="../styles/estilos.css">

</head>

</body>
<div id="page-container">

    <div class="col-md-8 mx-auto">

        <h1 class="titulos">Administrar Stock</h1>

        <div class="cajaCentrada">
        <br>
        <h2 class="titulos">Nuevo registro de stock </h2>
        <br>

        <?php
            include "modelo/Conexion.php";

            $conexion = new Conexion();
            $con = $conexion->conecta();

            //REALIZAMOS QUERY PARA OBTENER TODOS LOS PRODUCTOS
            $queryProductos = "select * from productos;";
            $listaProductos = mysqli_query($con, $queryProductos);

            //REALIZAMOS QUERY PARA OBTENER TODAS LAS SUCURSALES
            $querySucursales = "select * from sucursales";
            $listaSucursales = mysqli_query($con, $querySucursales);
        ?>

        <!-- FORMULARIO -->
        <form action="?controller=administrarstock&metodo=ingresar" method="POST" onsubmit="return validarDatosStock()"> 

            <div class="form-group">
                <label for="producto">Producto</label>
                <select name="producto" class="form-control" id="selProducto" required>
                    <option value="">seleccione producto</option>

                    <?php 
                        //generamos todas las opciones de los productos disponibles en la tabla productos
                        foreach( $listaProductos as $prod){
                            print  "<option value=".$prod["idProducto"].">".$prod["nombre"]."</option>";
                        }
                     ?>

                </select>
            </div>
            <br>

            <div class="form-group">
                <label for="sucursal">Sucursal</label>
                <select name="sucursal" class="form-control" id="selSucursal" required>
                    <option value="">seleccione sucursal</option>
                    <?php 
                        //generamos todas las opciones de las sucursales
                        foreach( $listaSucursales as $suc){
                            print  "<option value=".$suc["idSucursal"].">".$suc["nombre"]."</option>";
                        }
                    ?>
                </select>
            </div>
            <br>

            <div class="form-group">
                <label for="cantidad">Cantidad</label>
                <input type="number" min="0" class="form-control" id="cantidad" name="cantidad" required></input>        
            </div> 
            <br>

            <hr>
            <div class="info">
                <small>Formulario action="?controller=administrarstock&metodo=ingresar"</small></div>
            <input type="submit" class="btn btn-primary btnSubmit" value="Registrar stock"></input> 
            <div class="info">
                <small> Los datos de este formulario son recibidos en controlador/administrarstock.controlador.php -> ingresar() </small></div>
        </form>
        </div>
    </div>

    <div class="col-md-8 mx-auto">
        <div class="cajaCentrada">
            <form action="?controller=administrarstock&metodo=Listar" method="POST" > 
                <h1>Ver todos los registros de Stock </h1>    
                <br>
                <div class="info">
                    <small>Formulario action="?controller=administrarstock&metodo=Listar"</small></div>
                <input type="submit" class="btn btn-primary btnSubmit" value="Consultar" required></input> 
                <div class="info">
                    <small> Los datos de este formulario son recibidos en controlador/administrarstock.controlador.php -> Listar() </small></div>

            </form>
        </div> 
    </div>    

    <div class="codigo">
            <code class="codigo"> 
mysql> desc stock;
+-----------------+------+------+-----+---------+----------------+
| Field           | Type | Null | Key | Default | Extra          |
+-----------------+------+------+-----+---------+----------------+
| idRegistroStock | int  | NO   | PRI | NULL    | auto_increment |
| idProducto      | int  | YES  | MUL | NULL    |                |
| sucursal        | int  | YES  | MUL | NULL    |                |
| cantidad        | int  | YES  |     | NULL    |                |
+-----------------+------+------+-----+---------+----------------+
            </code>
        </div>

    <footer class="text-center">
        <small>Desarrollado por  Diego de la Fuente Curaqueo</small>
    </footer>
</div>
</body>

</html>
