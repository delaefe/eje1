<?php

require_once 'bd.php';

Class Categoria{

    //este objeto debe tener los campos de la tabla persona como variables
    private $nombre;
    private $idcategoria;

    private $pdo;

    public function __CONSTRUCT(){
        $this->pdo = BD::Conectar();
    } 


    /**
     * Get the value of idcategoria
     */
    public function getIdcategoria()
    {
        return $this->idcategoria;
    }

    /**
     * Set the value of idcategoria
     */
    public function setIdcategoria($idcategoria): self
    {
        $this->idcategoria = $idcategoria;

        return $this;
    }
}