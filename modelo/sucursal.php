<?php

require_once 'bd.php';

Class Sucursal{

    //este objeto debe tener los campos de la tabla persona como variables
 
    private $idsucursal;
    private $nombre;
    
    private $pdo;

    public function __CONSTRUCT(){
        $this->pdo = BD::Conectar();
    } 

    public function Listar(){
        try{
            $consulta = $this->pdo->prepare("SELECT * FROM sucursales");
            $consulta->execute();
            return $consulta->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die("error : ".$e->getMessage());
        }
    }
    /**
     * Get the value of idsucursal
     */
    public function getIdsucursal(){
        return $this->idsucursal;
    }

    /**
     * Set the value of idsucursal
     */
    public function setIdsucursal($idsucursal): self{
        $this->idsucursal = $idsucursal;

        return $this;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre(){
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     */
    public function setNombre($nombre): self{
        $this->nombre = $nombre;

        return $this;
    }
}