<?php
class InicioControlador{

    //TODAS LAS FUNCIONES ESTÁN RELACIONADAS CON LOS BOTONES DEL NAVBAR
    public function __CONSTRUCT(){
        //echo " INICIOCONTROLADOR: Controlador de entrada";
    }

    public function inicio(){
        //llama a la vista de inicio 
        require_once "vistas/cabecera.php";
        require_once "vistas/main.php";
    }

    public function registrarProducto(){
        require_once "vistas/cabecera.php";
        require_once "vistas/registrarProducto.php";
    }

    public function consultarProducto(){
        require_once "vistas/cabecera.php";
        require_once "vistas/consultarProducto.php";
    }

    public function consultarStock(){
        require_once "vistas/cabecera.php";
        require_once "vistas/administrarStock.php";
    }

}