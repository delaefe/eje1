<html>

    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                crossorigin="anonymous">
        <link rel="stylesheet" href="styles/estilos.css">
    </head>

    <body>
        
    <!--TABLA PARA MOSTRAR LA LISTA DE STOCK
        venimos de controlador/administrarstock.controlador.php -> buscar()
    -->

        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Producto</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Categoría</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Sucursal</th>

                </tr>
            </thead>
            <tbody>
                    <?php 
                        $registros = $this->modelo->Listar();
                        console_log($registros);

                        foreach($registros as $row){
                    ?>
                        <tr>
                            <td> <?= $row->Producto; ?>  </td>
                            <td> <?= $row->Precio_unidad; ?>      </td>
                            <td> <?= $row->Categoría; ?>   </td>
                            <td> <?= $row->cantidad; ?> </td>
                            <td> <?= $row->Sucursal; ?> </td>

                        </tr>
                    <?php
                        }
                    ?>
            </tbody>
        </table>

    </body>

</html>

