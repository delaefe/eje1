<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossorigin="anonymous">
    <link rel="stylesheet" href="../styles/estilos.css">

    <title>Administrador de Stock</title>
<!-- 
    ​Diseñar un software gestión de stock: 

    · Registrar un producto con los siguientes datos, id auto incrementable, código único del producto, nombre del producto, categoría, sucursal en la que se encuentra, y descripción, cantidad, y precio venta. 
    · Asignar productos a sucursal. (Defina 3 sucursales.) 
    · Consultar productos por código, nombre, y opcionalmente la sucursal. 
    · Dar de baja un producto. (Con la opción de eliminar el producto). 
    · Actualizar nombre, precio y descripción del producto. 
    · Crear un login para administrador del sistema. 

    ACTIVIDAD1 - Se solicita:  
    Cree las vistas que satisfagan los requerimientos (View) un entorno de desarrollo a selección (Netbeans o VS Code o similar), para ello cree un proyecto inicial, con una página de inicio y un dashboard para las opciones. 

    EVALUACION 1 - Se solicita:
    Diseñe un estilo visual o utilice una plantilla Bootstrap que se ajuste a los requerimientos. 
    Valide los campos obligatorios de las vistas y envíelos a una clase PHP la cual mostrará los datos por pantalla. 
  
    EV2 - se solicita:
    Se solicita:

    1- Insertar el registro de un producto nuevo en la base de datos. 
    2- Registrar un producto asociado a una sucursal. 
    3- Realizar la búsqueda de producto por código o nombre y opcionalmente la sucursal y mostrar los resultados.  (opcional)
-->
</head>

</body>
<div id="page-container">

        <?php
            include "modelo/Conexion.php";
            $conexion = new Conexion();
            $con = $conexion->conecta();
            $queryCategoria = "select * from categorias;";
            $listaCategoria = mysqli_query($con, $queryCategoria);
        ?>

        <!-- 
        Registrar un producto con los siguientes datos, id auto incrementable, código único del producto, nombre del producto, categoría, sucursal en la que se encuentra (Suponga 3), y descripción, cantidad, y precio venta. 
        -->
        
    <h1 class="titulos"> Registar nuevo producto</h1>
        <div class="col-md-8 mx-auto">
            <div class="cajaCentrada">
                <!--action="php/ingresaDatos.php" method="POST"-->

                <br>
                <h2 class="titulos"> Ingrese datos de producto</h1>
                <br>

                <form action="?controller=producto&metodo=ingresar" method="POST" onsubmit="return validarDatosRegistro()"> 
                    
                    <div class="form-group">
                        <label for="nombreProducto">Nombre del producto</label>
                        <input type="text" class="form-control" id="nombreProducto" name="nombreProducto" required></input>        
                        <br>
                    </div>
                    
                    <div class="form-group">
                        <label for="categoria">Categoria</label>
                        <select name="categoria" class="form-control" id="selCategoria" required>
                            <option value="">seleccione categoria</option>
                            <?php 
                                foreach( $listaCategoria as $prod){
                                    print  "<option value=".$prod["idCategoria"].">".$prod["nombre"]."</option>";
                                }
                            ?>
                        </select>
                        <br>
                    </div> 

                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" required></input>        
                        <br>
                    </div>

                    <div class="form-group">
                        <label for="precio">Precio</label>
                        <input type="number" class="form-control" id="precio" name="precio" min="0" required></input>        
                        <br><hr>
                    </div>

                    <div class="info">
                        <small>Formulario action="?controller=producto&metodo=ingresar"</small>
                        </div>
                    <input type="submit" class="btn btn-primary btnSubmit" value="ingresar"></input> 
                            
                    <div class="info">
                        <small>los datos son recibidos en controlador/producto.controlador.php -> inicio()</small>
                    </div>
                </form>
            </div>
        </div>

        <div class="codigo">
            <code class="codigo"> 
mysql> desc productos; //detalle tabla productos (local)
+-------------+-------------+------+-----+---------+----------------+
| Field       | Type        | Null | Key | Default | Extra          |
+-------------+-------------+------+-----+---------+----------------+
| idProducto  | int         | NO   | PRI | NULL    | auto_increment |
| nombre      | varchar(12) | YES  |     | NULL    |                |
| precio      | int         | YES  |     | NULL    |                |
| descripcion | varchar(50) | YES  |     | NULL    |                |
| categoria   | int         | YES  | MUL | NULL    |                |
+-------------+-------------+------+-----+---------+----------------+
            </code>
        </div>

    <footer class="text-center">
        <small>Desarrollado por Diego de la Fuente Curaqueo</small>
    </footer>
</div>
</body>

</html>
